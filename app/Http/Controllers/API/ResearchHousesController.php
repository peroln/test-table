<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ResearchHousesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $subRequest = DB::table('houses');

        if($request->input('name')){
            $subRequest->where('name','like','%'.$request->input('name').'%');
        }
        if($request->input('id')){
            $subRequest->where('id', $request->input('id'));
        }
        if($request->input('bedrooms')){
            $subRequest->where('bedrooms',$request->input('bedrooms'));
        }
        if($request->input('bathrooms')){
            $subRequest->where('bathrooms',$request->input('bathrooms'));
        }
        if($request->input('storeys')){

            $subRequest->where('storeys',$request->input('storeys'));
        }
        if($request->input('garages')){
            $subRequest->where('garages',$request->input('garages'));
        }
        if($request->input('price')){
            $price = $request->input('price');
            if(is_array($price)){
                $subRequest->whereBetween('price', [$price[0],$price[1]]);
            }

        }


        $result = $subRequest->get();
        return response()->json($result);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
